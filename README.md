# kafka-conumer-monitor

Node Js Application to Monitor Kafka Consumers

# How to build and run the application

1. Clone the repository and cd into the application directory
2. Run "npm install". This will download the node_modules for the project
3. Run "npm start". This will start the application
4. Go to http://localhost:8081 to view the Kafka Consumers

# Query Parameters

1. consumerName - Indicates the Kafka Consumer Name
2. env - Indicates the environment (Right now default is parallelb and other value is qa3)

Example - http://localhost:8081?consumerName=Comtrak&env=qa3 will provide details of the Comtrak Consumer in QA3
Example - http://localhost:8081?consumerName=Comtrak will provide details of the Comtrak Consumer in ParallelB