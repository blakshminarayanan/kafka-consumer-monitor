const http = require('http');
const kafkaNode = require('kafka-node');
const url = require('url');

const hostname = 'localhost';
const port = 8081;
var consumerName = '';

const clusterDetailQA3 = {
    broHost: "azuqlkafmq31.azu.insightglobal.net",
    broPort: "8192",
    cName: "QA3",
    cVersion: "1.0",
    chroot: "/"
}

const clusterDetailParallelB = {
    broHost: "azeslkafmq11.insightglobal.net",
    broPort: "8192",
    cName: "ParallelB",
    cVersion: "1.0",
    chroot: "/"
}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

const server = http.createServer((req, response) => {
    var q = url.parse(req.url, true).query;
    var clusterDetail = {};
    if (q.env && q.env === "qa3") {
        clusterDetail = clusterDetailQA3;
    } else {
        clusterDetail = clusterDetailParallelB;
    }
    if (q.consumerName) {
        consumerName = q.consumerName;
        //Get Consumer Details
        var nodeClient = new kafkaNode.KafkaClient({kafkaHost: clusterDetail.broHost+":"+clusterDetail.broPort});
        var admin = new kafkaNode.Admin(nodeClient);
        var consumerInfoList = [];
        admin.describeGroups([consumerName], (err, res) => {
            var groupId = res[consumerName]['groupId'];
            if (res[consumerName]['members'].length > 0) {
                var topicInfoList = res[consumerName]['members'][0]['memberAssignment']['partitions'];
                offset = new kafkaNode.Offset(nodeClient);
                var topicArray = Object.keys(topicInfoList);
                var startOffsetList, endOffsetList, consumerOffsetList = [];
                //To get Start Offset
                offset.fetchEarliestOffsets(topicArray, function (error, offsets) {
                    if (error)
                        console.log(JSON.stringify(error));
                    startOffsetList = offsets;
                });
                //To get End Offset
                offset.fetchLatestOffsets(topicArray, function (error, offsets) {
                    if (error)
                        console.log(JSON.stringify(error));
                    endOffsetList = offsets;
                });
                //To get Consumer Offset
                setTimeout(() => {
                    setTimeout (() => {
                        var consumerTopicList = [];
                        topicArray.forEach(topicName => {
                            var partitionArray = topicInfoList[topicName];
                            partitionArray.forEach(partitionName => {
                                var topicPartitionInfo = {
                                    topic: topicName,
                                    partition: partitionName
                                };
                                consumerTopicList.push(topicPartitionInfo);
                            });
                        });
                        offset.fetchCommitsV1(groupId, consumerTopicList, function (error, data) {
                            if (error)
                                console.log(JSON.stringify(error));
                            consumerOffsetList.push(data);
                        });
                        setTimeout(()=>{
                            topicArray.forEach(topicName => {
                                var partitionArray = topicInfoList[topicName];
                                partitionArray.forEach(partitionName => {
                                    var consumerInfoDoc = {
                                        Topic: topicName,
                                        Partition: partitionName,
                                        Start: startOffsetList[topicName][partitionName],
                                        End: endOffsetList[topicName][partitionName],
                                        Lag: 0
                                    };
                                    var lag = (consumerOffsetList[0][topicName][partitionName] === -1) ? 0 : endOffsetList[topicName][partitionName] - consumerOffsetList[0][topicName][partitionName];
                                    consumerInfoDoc.Lag = lag;
                                    consumerInfoList.push(consumerInfoDoc);
                                });
                            });
                            response.statusCode = 200;
                            response.setHeader('Content-Type', 'text/json');
                            response.end(JSON.stringify(consumerInfoList));
                        },500);
                    },500);
                },500);
            } else {
                response.statusCode = 500;
                response.setHeader('Content-Type', 'text/json');
                response.end(JSON.stringify('Oops!!! Consumer is down'));
            }
        });
        return;
    }
    if (url === '/favicon.ico') {
        response.writeHead(200, {'Content-Type': 'image/x-icon'} );
        response.end();
        return;
    }
    var nodeClient = new kafkaNode.KafkaClient({kafkaHost: clusterDetail.broHost+":"+clusterDetail.broPort});
    var admin = new kafkaNode.Admin(nodeClient);
    var consumerList = [];
    admin.listGroups((err, res) => {
        consumerList = Object.keys(res);
        consumerList.sort();
        response.statusCode = 200;
        response.setHeader('Content-Type', 'text/json');
        response.end(JSON.stringify(consumerList));
    });
  
});

server.listen(port, () => {
   console.log(`Server running at http://${hostname}:${port}/`);
});